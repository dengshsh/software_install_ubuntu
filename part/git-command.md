

# 查看分支

git branch -va

# 切换到dev分支

git checkout remotes/origin/dev

## 本地建一个分支对应

git checkout -b dev

# 创建一个新分支

```
1. git checkout -b ____local_name
//本地创建一个新分支
2. git push origin ____local_name:____remote_name
//把本地分支推到远端

注意，要检查下本地/远端分支名是否存在
git branch -va
```



# 修改文件

git add 修改的文件名

# 提取文件历史版本

```
git log
git log -- 
```



git log

git 

# 提交到本地版本库

git commit -m "修改log"

# 下载远程文件版本到本地文件

```shell
git pull origin dev:dev 

git pull origin AAA:aaa
## AAA为远程分支，   aaa为本地分支
```

## 强制更新本地
git pull -f origin dev:dev

# 提交到服器

```shell
git push origin dev:dev

git push origin aa:AAAA
## aa为本地分支，   AAAA为远程分支
```

# 当文件有冲突解决方案
1.git checkout  remotes/origin/dev
2.git checkout -b dev1 
3.git merge dev
4.git push origin dev1:dev

# git rebase使用

1.保证本地分支干净
2.git rebase origin xxx



## git拉取的不易出错的方式

	git stash					### 未提交的修改，保存到堆栈
	git fetch					### 拉取服务器上的文件
	git rebase origin/dev		### 变基
	git stash pop				### 从堆栈中取出文件

# 修改刚提交的注释

```
git commit --amend
```



## 若有冲突 xxx.cpp

3.手动解决冲突
4.git add xxx.cpp
5.git rebase --continue
6.git push origin xx:xx

git rm test.txt

# 本地有修改代码不想提交，但是想更新远程最新代码

1.git stash
2.git fetch
3.git rebase origin/dev  可能有冲突
4.git status
5.git stash pop  可能有冲突,解决冲突后执行git reset fileName  具体情况先查查

# 取消文件跟踪
git rm -r filepath

git rm --cached filepath

# 从git库里下载文件进行覆盖
git checkout -- filepath

## 强制覆盖所有文件
  git fetch --all
  git reset --hard origin/master
  git pull origin master

# git Merge 分支合并

当前是SAC822_2019，想合并dev

```
git checkout dev                                                            //切换到dev分支
git pull origin dev:dev                                                 //拉取dev分支
git checkout SAC822_2019                                       //切换到SAC822_2019分支
git merge dev                                                                 //合并dev分支
git push origin SAC822_2019:SAC822_2019     //推送合并后的SAC822_2019分支
```

# git 分支更名

```C++
git checkout old_name                                   //切换到原分支
git branch -m old_name new_name        //重命名分支
git push origin new_name:new_name    //提到远程分支// 
git push --delete origin old_name              //删除远程分支
```

```C++
//git push的一般形式为 git push <远程主机名> <本地分支名>  <远程分支名> 
//例如 git push origin master：master ，即是将本地的master分支推送到远程主机origin上的对应master分支

//第一个master是本地分支名，第二个master是远程分支名
```

# git 删除本地分支

```C++
git branch //查看本地分支列表
git branch -d 分支名称 //删除本地分支
```

# git 仓库太大了，怎么办

```
git clone --depth ?? -b ?? https://...
#先克隆某个版本，以及限制深度
git remote set-branches --add origin 'remote_branch_name'
git remote set-branches origin '*'
#设置你可能涉及到的分支，一个或者全部
git fetch -v
#这个命令让你摘取branch列表
git fetch --unshallow
#取得所有提交记录

```



# 创建一个git仓库后，会提供的命令

## 命令行指令

Git 全局设置
git config --global user.name ""
git config --global user.email ""

## 创建新版本库
git clone https://Dengss。。。。。。。。。。。。。。。。。。。。。
cd SAC822_Test
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

## 已存在的文件夹
cd existing_folder
git init
git remote add origin https://Dengss@。。。。。。。。。。。。。。。。。。。。。。。。。
git add .
git commit
git push -u origin master

## 已存在的 Git 版本库
cd existing_repo
git remote add origin https://Dengss@。。。。。。。。。。。。。。。。。
git push -u origin --all
git push -u origin --tags

# git 回退

```
git reset --hard commit_id
```


# git clone --depth 1

	git clone --depth 1 https://dengshsh@...................................git
	git remote set-branches origin remote_branch_name
	git fetch --depth 1 origin remote_branch_name
	git checkout remote_branch_name
	
# git  config

	git config --edit
	git config user.email "..........@..........."
	git config user.name "dengshsh"
	git config --global credential.helper cache			//记住密码（默认15分钟）
	git config credential.helper 'cache --timeout=3600'			//自定义密码存储时间
	git config --global credential.helper store				//长期存储密码