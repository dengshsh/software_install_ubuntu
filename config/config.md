# 使数字灯启动常量(小键盘)
```
sudo -i
su gdm -s /bin/bash
gsettings set org.gnome.settings-daemon.peripherals.keyboard numlock-state 'on'
```

# 修改提示符
```
sudo vim .bashrc
# /color_prompt
# 去除 \h\W
```

# 显示提示符
```
echo -e "\033[?25h" 
```
